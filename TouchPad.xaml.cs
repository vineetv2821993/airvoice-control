﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System;
using System.Collections.Generic;
using Windows.Foundation;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Xaml.Media;
using Windows.UI;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Windows8Theme
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TouchPad : Page
    {
        // A pointer back to the main page.  This is needed if you want to call methods in MainPage such
        // as NotifyUser()

        //Scenario specific constants
        const uint SUPPORTEDCONTACTS = 5;
        const double STROKETHICKNESS = 5;
        double X_RATIO, Y_RATIO; 
        uint numActiveContacts;
        Dictionary<uint, Point?> contacts;

        public TouchPad()
        {

            this.InitializeComponent();
            X_RATIO = 1366 / 640;
            Y_RATIO = 768 / 360;
            numActiveContacts = 0;
            contacts = new Dictionary<uint, Point?>((int)SUPPORTEDCONTACTS);
            Scenario1OutputRoot.PointerPressed += new PointerEventHandler(Scenario1OutputRoot_PointerPressed);
            Scenario1OutputRoot.PointerMoved += new PointerEventHandler(Scenario1OutputRoot_PointerMoved);
            Scenario1OutputRoot.PointerReleased += new PointerEventHandler(Scenario1OutputRoot_PointerReleased);
            Scenario1OutputRoot.PointerExited += new PointerEventHandler(Scenario1OutputRoot_PointerReleased);
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        protected void GoBack(object sender, RoutedEventArgs e)
        {
            // Use the navigation frame to return to the previous page
            if (this.Frame != null && this.Frame.CanGoBack) this.Frame.GoBack();
        }

        void Scenario1OutputRoot_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            uint ptrId = e.GetCurrentPoint(sender as FrameworkElement).PointerId;
            if (e.GetCurrentPoint(Scenario1OutputRoot).Properties.PointerUpdateKind != Windows.UI.Input.PointerUpdateKind.Other)
            {
                this.buttonPress.Text = e.GetCurrentPoint(Scenario1OutputRoot).Properties.PointerUpdateKind.ToString();
            }
            if (contacts.ContainsKey(ptrId))
            {
                contacts[ptrId] = null;
                contacts.Remove(ptrId);
                --numActiveContacts;
            }
            e.Handled = true;
        }
        
        void Scenario1OutputRoot_PointerMoved(object sender, PointerRoutedEventArgs e)
        {

            Windows.UI.Input.PointerPoint pt = e.GetCurrentPoint(Scenario1OutputRoot);
            uint ptrId = pt.PointerId;
            if (pt.Properties.PointerUpdateKind != Windows.UI.Input.PointerUpdateKind.Other)
            {
                this.buttonPress.Text = pt.Properties.PointerUpdateKind.ToString();
            }
            if (contacts.ContainsKey(ptrId) && contacts[ptrId].HasValue)
            {
                Point currentContact = pt.Position;
                Point previousContact = contacts[ptrId].Value;
                if (Distance(currentContact, previousContact) > 4)
                {
                    Line l = new Line()
                    {
                        X1 = previousContact.X,
                        Y1 = previousContact.Y,
                        X2 = currentContact.X,
                        Y2 = currentContact.Y,
                        StrokeThickness = STROKETHICKNESS,
                        Stroke = new SolidColorBrush(toggleColor),
                        StrokeEndLineCap = PenLineCap.Round
                    };
                    WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Touchpad, Math.Round(currentContact.X - previousContact.X) + " " + Math.Round(previousContact.Y - currentContact.Y));
                    contacts[ptrId] = currentContact;
                    ((System.Collections.Generic.IList<UIElement>)Scenario1OutputRoot.Children).Add(l);
                }
            }

            e.Handled = true;
        }

        private double Distance(Point currentContact, Point previousContact)
        {
            return Math.Sqrt(Math.Pow(currentContact.X - previousContact.X, 2) + Math.Pow( previousContact.Y-currentContact.Y, 2));
        }

        void Scenario1OutputRoot_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            if (numActiveContacts >= SUPPORTEDCONTACTS)
            {
                // cannot support more contacts
                return;
            }
            Windows.UI.Input.PointerPoint pt = e.GetCurrentPoint(Scenario1OutputRoot);
            if (pt.Properties.PointerUpdateKind != Windows.UI.Input.PointerUpdateKind.Other)
            {
                this.buttonPress.Text = pt.Properties.PointerUpdateKind.ToString();
            }
            contacts[pt.PointerId] = pt.Position;
            e.Handled = true;
            ++numActiveContacts;
        }

        void Scenario1Reset(object sender, RoutedEventArgs e)
        {
            Scenario1Reset();
        }

        public void Scenario1Reset()
        {
            ((System.Collections.Generic.IList<UIElement>)Scenario1OutputRoot.Children).Clear();
            numActiveContacts = 0;
            if (contacts != null)
            {
                contacts.Clear();
            }
        }

        private static Color toggleColor = Colors.Red;
        private void lineToggleVal(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (lineToggle.IsOn)
                    toggleColor = Colors.Black;
            if (!lineToggle.IsOn)
                    toggleColor = Colors.Red;


        }

        private void leftMouseClick(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, WindowsRTClient.CommandValue.TouchpadButton.LEFT_CLICK);
        }

        private void rightMouseClick(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, WindowsRTClient.CommandValue.TouchpadButton.RIGHT_CLICK);
        }

    }
}

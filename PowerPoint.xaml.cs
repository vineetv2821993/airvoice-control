﻿using MjpegProcessor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Windows8Theme
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PowerPoint : Page
    {
        private readonly MjpegDecoder _mjpeg;
        private readonly WriteableBitmap _bmp = new WriteableBitmap(1, 1);
        public PowerPoint()
        {
            this.InitializeComponent();
            _mjpeg = new MjpegDecoder();
            _mjpeg.FrameReady += mjpeg_FrameReady;
            _mjpeg.Error += _mjpeg_Error;
            _mjpeg.ParseStream(new Uri("http://" + WindowsRTClient.IpAddress + ":12120/"));
        }
        private async void mjpeg_FrameReady(object sender, FrameReadyEventArgs e)
        {
            InMemoryRandomAccessStream stream = new InMemoryRandomAccessStream();
            await stream.WriteAsync(e.FrameBuffer);
            stream.Seek(0);
            _bmp.SetSource(stream);
            image.Source = _bmp;
        }

        async void _mjpeg_Error(object sender, ErrorEventArgs e)
        {
            await new MessageDialog(e.Message).ShowAsync();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        protected void GoBack(object sender, RoutedEventArgs e)
        {
            // Use the navigation frame to return to the previous page
            if (this.Frame != null && this.Frame.CanGoBack) this.Frame.GoBack();
        }

        private void nextSlideClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, WindowsRTClient.CommandValue.PowerPoint.NEXT_SLIDE);
        }

        private void prevSlideClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, WindowsRTClient.CommandValue.PowerPoint.PREVIOUS_SLIDE);
        }

        private void zoomInClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, WindowsRTClient.CommandValue.PowerPoint.ZOOM_IN);
        }

        private void zoomOutClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, WindowsRTClient.CommandValue.PowerPoint.ZOOM_OUT);
        }

        private void fullscreenClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, WindowsRTClient.CommandValue.PowerPoint.FULLSCREEN);
        }

        private void exitFullClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, WindowsRTClient.CommandValue.PowerPoint.EXIT_FULLSCREEN);
        }

        private void presenterViewClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, WindowsRTClient.CommandValue.PowerPoint.PRESENTER_VIEW);
        }

        async private void jumpPageClick(object sender, RoutedEventArgs e)
        {
            char [] pageNumber;
            bool caughtException = false;
            try {
                pageNumber = Convert.ToInt32(pageInput.Text).ToString().ToCharArray();
                for (int i = 0; i < pageNumber.Length; i++)
                {
                    WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, pageNumber[i].ToString());
                }
                WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, WindowsRTClient.CommandValue.PowerPoint.JUMP_EVENT);
            }
            catch(Exception eN){
                caughtException = true;
            }
            if (caughtException == true)
            {
                var dlg = new MessageDialog("Invalid Input\n Please Enter Number Only.");
                await dlg.ShowAsync();
            }
            
        }
    }
}

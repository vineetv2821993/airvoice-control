﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Windows8Theme
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PCConfigure : Page
    {
        public PCConfigure()
        {
            this.InitializeComponent();
            resolutionCombo.SelectedIndex = 0;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        protected void GoBack(object sender, RoutedEventArgs e)
        {
            // Use the navigation frame to return to the previous page
            if (this.Frame != null && this.Frame.CanGoBack) this.Frame.GoBack();
        }

        private void screenToggleVal(object sender, RoutedEventArgs e)
        {
            if (screenToggle.IsOn)
                WindowsRTClient.SendRequest(WindowsRTClient.CommandType.BrightnessScreenToggle, WindowsRTClient.CommandValue.SystemBasic.BrightnessScreenToggle.SCREEN_TURN_OFF);
            if (!screenToggle.IsOn)
                WindowsRTClient.SendRequest(WindowsRTClient.CommandType.BrightnessScreenToggle, WindowsRTClient.CommandValue.SystemBasic.BrightnessScreenToggle.SCREEN_TURN_ON);
 
        }
        private void masterToggleVal(object sender, RoutedEventArgs e)
        {
            if (masterToggle.IsOn)
                WindowsRTClient.SendRequest(WindowsRTClient.CommandType.MasterVolumeMuteToggle, WindowsRTClient.CommandValue.SystemBasic.MasterVolumeMuteToggle.MUTE);
            if (!masterToggle.IsOn)
                WindowsRTClient.SendRequest(WindowsRTClient.CommandType.MasterVolumeMuteToggle, WindowsRTClient.CommandValue.SystemBasic.MasterVolumeMuteToggle.UNMUTE);
        }
        private void masterSliderValChange(object sender, RangeBaseValueChangedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.MasterVolume, Convert.ToString(e.NewValue));
        }

        private void brightnessSliderValChange(object sender, RangeBaseValueChangedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Brightness, Convert.ToString(e.NewValue));
        }

        private void clearClipClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.SystemBasic, WindowsRTClient.CommandValue.SystemBasic.Clipboard.CLEAR);
        }

        private void speakClipClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.SystemBasic, WindowsRTClient.CommandValue.SystemBasic.Clipboard.SPEAK);
        }

        private void shutdownClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.SystemBasic, WindowsRTClient.CommandValue.SystemBasic.SHUTDOWN);
        }

        private void restartClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.SystemBasic, WindowsRTClient.CommandValue.SystemBasic.RESTART);
        }

        private void logOffClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.SystemBasic, WindowsRTClient.CommandValue.SystemBasic.LOG_OFF);
        }

        private void screensaverClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.SystemBasic, WindowsRTClient.CommandValue.SystemBasic.SCREENSAVER);
        }

        private void hibernateClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.SystemBasic, WindowsRTClient.CommandValue.SystemBasic.HIBERNATE);
        }

        private void fhibernateClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.SystemBasic, WindowsRTClient.CommandValue.SystemBasic.HIBERNATE_FORCE);
        }

        private void fshutdownClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.SystemBasic, WindowsRTClient.CommandValue.SystemBasic.SHUTDOWN_FORCE);
        }

        private void frestartClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.SystemBasic, WindowsRTClient.CommandValue.SystemBasic.RESTART_FORCE);
        }

        private void minimizeAllClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, WindowsRTClient.CommandValue.SystemBasic.MINIMIZE_ALL);
        }

        private void closeCurrentClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, WindowsRTClient.CommandValue.SystemBasic.CLOSE_CURRENT_WINDOW);
        }

        private void changeResClick(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Resolution, WindowsRTClient.CommandValue.SystemBasic.Resolution.RES[resolutionCombo.SelectedIndex]);
        }
    }
}

﻿using System;
using System.Linq;
using MjpegProcessor;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Windows8Theme
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Mirror : Page

    {
        private readonly MjpegDecoder _mjpeg;
        private readonly WriteableBitmap _bmp = new WriteableBitmap(1, 1);
        public Mirror()
        {
            this.InitializeComponent();
            _mjpeg = new MjpegDecoder();
            _mjpeg.FrameReady += mjpeg_FrameReady;
            _mjpeg.Error += _mjpeg_Error;
            _mjpeg.ParseStream(new Uri("http://"+WindowsRTClient.IpAddress+":12120/"));

        }
        protected void GoBack(object sender, RoutedEventArgs e)
        {
            // Use the navigation frame to return to the previous page
            if (this.Frame != null && this.Frame.CanGoBack) this.Frame.GoBack();
        }
       private async void mjpeg_FrameReady(object sender, FrameReadyEventArgs e)
		{
			InMemoryRandomAccessStream stream = new InMemoryRandomAccessStream();
			await stream.WriteAsync(e.FrameBuffer);
			stream.Seek(0);
			_bmp.SetSource(stream);
			image.Source = _bmp;
		}

		async void _mjpeg_Error(object sender, ErrorEventArgs e)
		{
			await new MessageDialog(e.Message).ShowAsync();
		}
	}


        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
}

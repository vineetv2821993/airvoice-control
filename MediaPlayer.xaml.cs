﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System;
using System.Linq;
using MjpegProcessor;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Windows8Theme
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MediaPlayer : Page
    {
        private readonly MjpegDecoder _mjpeg;
        private readonly WriteableBitmap _bmp = new WriteableBitmap(1, 1);
        public MediaPlayer()
        {
            this.InitializeComponent();
            _mjpeg = new MjpegDecoder();
            _mjpeg.FrameReady += mjpeg_FrameReady;
            _mjpeg.Error += _mjpeg_Error;
            _mjpeg.ParseStream(new Uri("http://" + WindowsRTClient.IpAddress + ":12120/"));
        }
        private async void mjpeg_FrameReady(object sender, FrameReadyEventArgs e)
        {
            InMemoryRandomAccessStream stream = new InMemoryRandomAccessStream();
            await stream.WriteAsync(e.FrameBuffer);
            stream.Seek(0);
            _bmp.SetSource(stream);
            image.Source = _bmp;
        }

        async void _mjpeg_Error(object sender, ErrorEventArgs e)
        {
            await new MessageDialog(e.Message).ShowAsync();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        protected  void GoBack(object sender, RoutedEventArgs e)
        {
            // Use the navigation frame to return to the previous page
            if (this.Frame != null && this.Frame.CanGoBack) this.Frame.GoBack();
        }
        private void pressMediaKey(String CommandValue){
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Keyboard, CommandValue);
        }

        private void playTap(object sender, TappedRoutedEventArgs e)
        {
            pressMediaKey(WindowsRTClient.CommandValue.MediaPlayer.PLAY);
        }

        private void stopTap(object sender, TappedRoutedEventArgs e)
        {
            pressMediaKey(WindowsRTClient.CommandValue.MediaPlayer.STOP);
        }

        private void prevTap(object sender, TappedRoutedEventArgs e)
        {
            pressMediaKey(WindowsRTClient.CommandValue.MediaPlayer.PREVIOUS);
        }

        private void slowTap(object sender, TappedRoutedEventArgs e)
        {
            pressMediaKey(WindowsRTClient.CommandValue.MediaPlayer.SLOW);
        }

        private void nextTap(object sender, TappedRoutedEventArgs e)
        {
            pressMediaKey(WindowsRTClient.CommandValue.MediaPlayer.NEXT);
        }

        private void fastTap(object sender, TappedRoutedEventArgs e)
        {
            pressMediaKey(WindowsRTClient.CommandValue.MediaPlayer.FAST);
        }

        private void suffleTap(object sender, TappedRoutedEventArgs e)
        {
            pressMediaKey(WindowsRTClient.CommandValue.MediaPlayer.SUFFLE);
        }

        private void repeatTap(object sender, TappedRoutedEventArgs e)
        {
            pressMediaKey(WindowsRTClient.CommandValue.MediaPlayer.REPEAT);
        }

        private void masterSliderValChange(object sender, RangeBaseValueChangedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.MasterVolume, Convert.ToString(e.NewValue));
        }
        static int initProgress = 0;
        private void playerSliderValChange(object sender, RangeBaseValueChangedEventArgs e)
        {
            int progress = (int) e.NewValue;
            int progD = progress - initProgress;
            int loopN = 0;
            String change = "";
            if (progress == 0)
            {
                loopN = 10;
                change = WindowsRTClient.CommandValue.MediaPlayer.PLAYER_VOLUME_DOWN;
            }
            else if (progress == 10)
            {
                loopN = 10;
                change = WindowsRTClient.CommandValue.MediaPlayer.PLAYER_VOLUME_UP;
            }
            else if (progD > 0)
            {
                loopN = progD;
                change = WindowsRTClient.CommandValue.MediaPlayer.PLAYER_VOLUME_UP;
            }
            else if (progD < 0)
            {
                loopN = -progD;
                change = WindowsRTClient.CommandValue.MediaPlayer.PLAYER_VOLUME_DOWN;
            }
            // TODO Auto-generated method stub

            for (int i = 0; i < loopN; i++)
            {
                pressMediaKey(change);
            }
            initProgress = progress;

        }

        private void masterToggleVal(object sender, RoutedEventArgs e)
        {
            if(masterToggle.IsOn)
                WindowsRTClient.SendRequest(WindowsRTClient.CommandType.MasterVolumeMuteToggle, WindowsRTClient.CommandValue.SystemBasic.MasterVolumeMuteToggle.MUTE);
            if(!masterToggle.IsOn)
                WindowsRTClient.SendRequest(WindowsRTClient.CommandType.MasterVolumeMuteToggle, WindowsRTClient.CommandValue.SystemBasic.MasterVolumeMuteToggle.UNMUTE);
        }

        private void playerToggleVal(object sender, RoutedEventArgs e)
        {
            pressMediaKey("F7");
        }

        private void OpenPlayer_Click(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.ExecutePlayer, WindowsRTClient.CommandValue.NULL);
        }

        private void ClosePlayer_Click(object sender, RoutedEventArgs e)
        {
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Console, WindowsRTClient.CommandValue.Console.KILL_PLAYER);
        }
    }
}

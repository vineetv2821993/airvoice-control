﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Windows8Theme
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AppConfigure : Page
    {
        public AppConfigure()
        {
            this.InitializeComponent();
            InputScopeNameValue digitInputNameValue = InputScopeNameValue.Number;
            serverPortText.InputScope = new InputScope() { 
                Names = { new InputScopeName() { NameValue = digitInputNameValue}}
            };
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        protected void GoBack(object sender, RoutedEventArgs e)
        {
            // Use the navigation frame to return to the previous page
            if (this.Frame != null && this.Frame.CanGoBack) this.Frame.GoBack();
        }

        async private void saveSettings_Click(object sender, RoutedEventArgs e)
        {
            String Message;
            var dlg = new MessageDialog("");
            try
            {
                WindowsRTClient.IpAddress = serverIPText.Text;
                WindowsRTClient.Port = Convert.ToInt32(serverPortText.Text);
                Message = "Server Settings Saved\n IP Address: " + WindowsRTClient.IpAddress + "\n Port: " + WindowsRTClient.Port;

            }
            catch (FormatException eFE) {
                Message = "Error!\nPort accept number only.";
            }
            catch (Exception eF)
            {
                Message = "Error!\nCheck your Settings..";
            }
            dlg = new MessageDialog(Message);
            await dlg.ShowAsync();
            
        }

        async private void clickConnect_Click(object sender, RoutedEventArgs e)
        {
            String Response = "";
            String Message = "";
           // WindowsRTClient.Response = "";
            try
            {
                WindowsRTClient.IpAddress = serverIPText.Text;
                WindowsRTClient.Port = Convert.ToInt32(serverPortText.Text);
            }
            catch (Exception eF)
            {
                Message = "Error!\nCheck your Settings..";
            }
            if (Message != "")
            {
                var dlg = new MessageDialog(Message);
                await dlg.ShowAsync();
            }

            
            WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Connect, "Connecting");

            if (WindowsRTClient.Response == "Error")
                connectText.Text = "Invalid Ip Address:Port";
            else if (WindowsRTClient.Response == "")
                connectText.Text = "No Server Found";
            else
                connectText.Text = "Connected";
        }
    }
}

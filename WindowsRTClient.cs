﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.UI.Popups;

namespace Windows8Theme
{
    class WindowsRTClient
    {
        public static String IpAddress = "127.0.0.1";
        public static int Port = 1993;
        public static Dictionary<String,int> ServerInfo = new Dictionary<string, int>();
        public static String Response{set;get;}
        public static async void SendRequest(CommandType CT, String CommandValue)
        {
            try
            {
                var tcpClient = new StreamSocket();
                await tcpClient.ConnectAsync(new HostName(IpAddress), Port.ToString());
                var writer = new DataWriter(tcpClient.OutputStream);
                writer.WriteString(CT + "." + CommandValue);
                await writer.StoreAsync();
                await writer.FlushAsync();
            }
            catch (Exception) { }
        }

        public static void saveServerList(String file, String ip, int port)
        {
            ServerInfo.Add(ip, port);
            
            //overwritefile
        
        }
        public static void readServerList(String file, Dictionary<String, int> serverInfo)
        {


        }
        public static async void SendRequestHttp(CommandType CT, String CommandValue) 
        {
            if (IpAddress == null || Port == 0)
            {
                Response = "Error";
            }
            else
            {
                HttpContent msg = new StringContent("");
                HttpClientHandler handler = new HttpClientHandler();
                msg.Headers.Add("CT", CT.ToString());
                msg.Headers.Add("CV", CommandValue);
                msg.Headers.Remove("Content-Type");

                HttpClient httpClient = new HttpClient(handler);
                Uri url = new Uri("http://" + IpAddress + ":" + Port + "/");
                try
                {
                    HttpResponseMessage response = await httpClient.PostAsync(url, msg);

                    System.Diagnostics.Debug.WriteLine("response: " + response);
                    string responseAsString = await response.Content.ReadAsStringAsync();

                    Response = responseAsString;
                    var dlg = new MessageDialog(responseAsString);
                    await dlg.ShowAsync();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                }
            }
        }
        public static string SendRequestResponse(CommandType CT, String CommandValue) {
            SendRequest(CT, CommandValue);
            return Response;
        }
        public enum CommandType { Connect, MasterVolume, MasterVolumeMuteToggle, Brightness, BrightnessScreenToggle, CDRom, Clipboard, Resolution, Keyboard, Touchpad, TouchpadButton, TouchpadWheel, Console, ConsoleTool, SystemBasic, ExecutePlayer}
        public class CommandValue{
            public const string NULL = "";
            //CommandType = MasterVolume, Brightness, Touchpad, TouchpadWheel having Dynamic CommandValue

            public class Connect {
                //CommandType = Connect
                public const string CONNECT = "Connecting";
            }
            public class Console { 
                //CommandType = Console
                public class ConsoleTool {
                    public const string EXECUTE_PLAYER = "exec max \"C:\\Program Files\\Windows Media Player\\wmplayer.exe\"";
                }
                
                public const string KILL_PLAYER = "taskkill /f /im wmplayer.exe";
            }
            public class MediaPlayer {
                //CommandType = Keyboard
                public const string PLAY = "ctrl+p";
                public const string STOP = "ctrl+s";
                public const string PREVIOUS = "ctrl+f";
                public const string NEXT = "ctrl+b";
                public const string SLOW = "ctrl+shift+b";
                public const string FAST = "ctrl+shift+f";
                public const string SUFFLE = "ctrl+h";
                public const string REPEAT = "ctrl+t";
                public const string PLAYER_VOLUME_UP = "F9";
                public const string PLAYER_VOLUME_DOWN = "F8";
            }

            public class PowerPoint {
                //CommandType = Keyboard
                public const string NEXT_SLIDE = "pagedown";
                public const string PREVIOUS_SLIDE = "pageup";
                public const string JUMP_EVENT = "enter";
                //Powerpoint number+JUMP_EVENT e.g to Jump to Page 11, "1+1+enter"
                public const string ZOOM_IN = "ctrl+plus";
                public const string ZOOM_OUT = "ctrl+minus";
                public const string FULLSCREEN = "F5";
                public const string PRESENTER_VIEW = "alt+F5";
                public const string EXIT_FULLSCREEN = "esc";
            }
            public class TouchpadButton
            {
                //CommandType = TouchpadButton
                public const string LEFT_CLICK = "left click";
                public const string RIGHT_CLICK = "right click";
                public const string MIDDLE_CLICK = "middle click";
                public const string LEFT_DBL_CLICK = "left dblclick";
                public const string RIGHT_DBL_CLICK = "right dblclcik";
                public const string MIDDLE_DBL_CLICK = "middle dbl click";
            }

            public class SystemBasic {
                //CommandType = SystemBasic
                public const string SHUTDOWN = "exitwin shutdown";
                public const string POWER_OFF = "exitwin poweroff";
                public const string RESTART = "exitwin reboot";
                public const string LOG_OFF = "exitwin logoff";
                public const string HIBERNATE = "hibernate";
                public const string SCREENSAVER = "screensaver";
                public const string SHUTDOWN_FORCE = "exitwin shutdown force";
                public const string POWER_OFF_FORCE = "exitwin poweroff force";
                public const string RESTART_FORCE = "exitwin reboot force";
                public const string LOG_OFF_FORCE = "exitwin logoff forceifhung";
                public const string HIBERNATE_FORCE = "hibernate force";

                public const string MINIMIZE_ALL = "rwin+m";
                public const string CLOSE_CURRENT_WINDOW = "alt+F4";
                
                public class MasterVolumeMuteToggle
                {
                    //CommandType = MasterVolumeMuteToggle
                    public const string MUTE = "mutesysvolume 1";
                    public const string UNMUTE = "mutesysvolume 0";
                }
                public class BrightnessScreenToggle
                {
                    //CommandType = BrightnessScreenToggle
                    public const string SCREEN_TURN_OFF = "monitor off";
                    public const string SCREEN_TURN_ON = "monitor on";
                }
                public class CDRom
                {
                    //CommandType = CDRom
                    public const string OPEN = "cdrom open";
                    public const string CLOSE = "cdrom close";
                }

                public class Resolution
                {
                    //CommandType = Resolution
                    public const string RES_800_600_16 = "setdisplay 800 600 16";
				    public const string RES_800_600_32 = "setdisplay 800 600 32";
				    public const string RES_1024_768_16 = "setdisplay 1024 768 16";
				    public const string RES_1024_768_32 = "setdisplay 1024 768 32";
				    public const string RES_1280_720_32 = "setdisplay 1280 720 32";
				    public const string RES_1366_768_32 = "setdisplay 1366 768 32";
                    public const string RES_1920_1080_32 = "setdisplay 1920 1080 32";

                    public static string[] RES = {
                        "setdisplay 800 600 16",
				        "setdisplay 800 600 32",
				        "setdisplay 1024 768 16",
				        "setdisplay 1024 768 32",
				        "setdisplay 1280 720 32",
				        "setdisplay 1366 768 32",
                        "setdisplay 1920 1080 32"
                    };
                }
                public class Clipboard
                {
                    //CommandType = Clipboard
                    public const string SPEAK = "speak text ~$clipboard$";
                    public const string CLEAR = "clipboard clear";
                }
            }

        }

    }
}

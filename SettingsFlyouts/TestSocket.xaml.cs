﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Windows8Theme.SettingsFlyouts
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TestSocket : Page
    {
        public TestSocket()
        {
            this.InitializeComponent();
  

        }
        public async void TestBuddy() {
            TcpClient myClient = new TcpClient();
            IPEndPoint myEP = new IPEndPoint(new IPAddress("10.8.40.251"), 11000);
           myClient.Connect(myEP);
            NetworkStream myStream = myClient.GetStream();
            String str = "Hello";
            byte[] myarray = GetBytes(str);
            myStream.Write(myarray,0,myarray.Length);
            myClient.Close();

        }
        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        public async void TestSomething()
       {
            //  Setup local server
            //
       /*     StreamSocketListener listener = new StreamSocketListener();
            listener.ConnectionReceived += async (sender, args) =>
            {
                DataReader serverReader = new DataReader(args.Socket.InputStream);
                await serverReader.LoadAsync(4096);   //  <-- Exception on this line
            };
        * */


           
         //  IPEndPoint EndPt = new IPEndPoint(new IPAddress("10.8.40.251"),12128);
          //  HostName Hst = EndPt.Address.GetHostNameObject();

            //  Setup client
            //
        /*    StreamSocket socket = new StreamSocket();
           // await socket.BindServiceNameAsync("12129");
            
            HostName remoteHost = new HostName("10.8.40.251");
               await socket.ConnectAsync(remoteHost, "3333");
 
                DataWriter writer = new DataWriter(socket.OutputStream);
                writer.WriteString("Hello");
                await writer.StoreAsync();
   
               // socket.Dispose();
         * */
            // Create a new socket and bind it to a local port
//DatagramSocket udpSocket = new DatagramSocket();
//await udpSocket.BindServiceNameAsync("5556");
StreamSocket udpSocket = new StreamSocket();

// Open a connection to a remote host
HostName remoteHost = new HostName("10.8.40.251");
await udpSocket.ConnectAsync(remoteHost, "11000");

// Send a data string to the remote host in a UDP packet
DataWriter udpWriter = new DataWriter(udpSocket.OutputStream);
udpWriter.WriteString("juncti juvant");
await udpWriter.StoreAsync();
            
            
        }


      public async Task Listen(DataReader reader)
        {
            await reader.LoadAsync(4096);
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //TestSomething();
            //TestBuddy();
            //TestHTTP();
            tcpSocketTest();
            //WindowsRTClient.IpAddress="127.0.0.1";
            //WindowsRTClient.Port = 1993;
            //WindowsRTClient.SendRequest(WindowsRTClient.CommandType.Console,"cdrom open");

        }
        public async void tcpSocketTest() {

            var tcpClient = new StreamSocket();
            await tcpClient.ConnectAsync(new HostName("localhost"), "3001");
            var writer = new DataWriter(tcpClient.OutputStream);
            writer.WriteString("ISCP\0\0\0\x10\0\0\0....");
            writer.StoreAsync();
            writer.FlushAsync();
        }
        public async void TestHTTP() {
            HttpContent msg = new StringContent("a");
            HttpClientHandler handler = new HttpClientHandler();
            msg.Headers.Add("Command","PC Commands Put Here");
            //msg.Headers.Remove("Content-Type");

            
         /*   handler.UseDefaultCredentials = true;
            handler.UseCookies = true;
            handler.CookieContainer = new System.Net.CookieContainer();
            */
            
           

            HttpClient httpClient = new HttpClient(handler);
            
            //HttpContent msg = new StringContent("");


            // Assign the authentication headers
            //httpClient.DefaultRequestHeaders.Authorization = CreateBasicHeader("username", "password");
           // System.Diagnostics.Debug.WriteLine("httpClient.DefaultRequestHeaders.Authorization: " + httpClient.DefaultRequestHeaders.Authorization);


            // Call out to the site
            Uri url = new Uri("http://101.215.35.78:1993/");
            HttpResponseMessage response = await httpClient.PostAsync(url, msg);
            //HttpResponseMessage response = await httpClient.GetAsync("http://127.0.0.1:1993");
            System.Diagnostics.Debug.WriteLine("response: " + response);
            string responseAsString = await response.Content.ReadAsStringAsync();
            var dlg = new MessageDialog(responseAsString );

            await dlg.ShowAsync();
        }
        public async void TestExcel() { 
        /*DatagramSocket multicastsocket = new DatagramSocket();
//multicastsocket.MessageReceived += multicastsocket.MessageReceived;

HostName multicastGroup = new HostName("10.8.40.251");
multicastsocket.BindServiceNameAsync("52200");
multicastsocket.JoinMulticastGroup(multicastGroup);



var socket = new DatagramSocket();
//HostName multicastGroup = new HostName("10.8.40.251");

using(var oStream = socket.GetOutputStreamAsync(multicastGroup, "52200"))
{
    using(var writer = new DataWriter(await oStream))
    {
        writer.WriteString("Hello Test");
        await writer.StoreAsync();
    }
}
            */
            // Create a request using a URL that can receive a post. 
  
        }
    }
}
